import pygame
import random
import os
import math
from config import *
pygame.init()

win = pygame.display.set_mode((1000, 800))
pygame.display.set_caption("game")

floatRight = [pygame.image.load(os.path.join(
    'img', 'R.png')), pygame.image.load(os.path.join('img', 'R2.png'))]
floatLeft = [pygame.image.load(os.path.join(
    'img', 'L.png')), pygame.image.load(os.path.join('img', 'L2.png'))]
floatUp = [pygame.image.load(os.path.join('img', 'U.png')), pygame.image.load(
    os.path.join('img', 'U2.png'))]
floatDown = [pygame.image.load(os.path.join(
    'img', 'D.png')), pygame.image.load(os.path.join('img', 'D2.png'))]
stand = [pygame.image.load(os.path.join('img', 'U.png')),
         pygame.image.load(os.path.join('img', 'D2.png'))]
fixed = pygame.image.load(os.path.join('img', 'FM.png'))
land = pygame.image.load(os.path.join('img', 'land.png'))

pacleft = pygame.image.load(os.path.join('img', 'ML.png'))
pacright = pygame.image.load(os.path.join('img', 'MR.png'))

collisionsound = pygame.mixer.Sound(os.path.join('sound', 'col.wav'))
monster = pygame.mixer.Sound(os.path.join('sound', 'monster.wav'))
music = pygame.mixer.music.load(os.path.join('sound', 'music.mp3'))

pygame.mixer.music.play(-1)

# player


class ghost(object):
    def __init__(self, x, y, width, height, index):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.vel = 3
        self.left = False
        self.right = False
        self.up = False
        self.down = False
        self.score = 0
        self.hitbox = (self.x+3, self.y+3, 43, 45)
        self.index = index

    def draw(self, win):

        self.hitbox = (self.x+3, self.y+3, 43, 45)

        if self.left:
            win.blit(floatLeft[self.index], (self.x, self.y))
        elif self.right:
            win.blit(floatRight[self.index], (self.x, self.y))
        elif self.up:
            win.blit(floatUp[self.index], (self.x, self.y))
        elif self.down:
            win.blit(floatDown[self.index], (self.x, self.y))
        else:
            win.blit(stand[self.index], (self.x, self.y))

    def collide(self, num):
        self.score -= 10
        if num == 1:
            collisionsound.play()
        else:
            monster.play()

# moving obstacles


class pacman(object):
    vel = 5

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.hitbox = (self.x+5, self.y+5, 50, 50)
        self.left = False

    def draw(self, win):
        if not(self.left):
            if self.x + self.vel > 900:
                self.vel = -self.vel
                self.left = True
        else:
            if self.x + self.vel < 40:
                self.vel = -self.vel
                self.left = False

        self.hitbox = (self.x+5, self.y+5, 50, 50)
        if self.left:
            win.blit(pacleft, (self.x, self.y))
        else:
            win.blit(pacright, (self.x, self.y))
        self.x += self.vel

# fixed obstacles


class stone(object):
    width = 60
    height = 70

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.hitbox = (self.x, self.y, 60, 60)

    def draw(self, win):
        self.hitbox = (self.x, self.y, 60, 60)
        win.blit(fixed, (self.x, self.y))


def drawobj():
    win.fill((0, 0, 255))
    text = font.render('Score: ' + str(p.score), 1, black)
    # land
    for i in range(6):
        win.blit(land, (0, i*150))

    # fixed obstacles
    for i in range(6):
        fix[i].draw(win)
        # pygame.display.update()

    # pacmans
    for i in range(5):
        pac[i].draw(win)
        # pygame.display.update()

    if p == p1:
        win.blit(text, (850, 761))
    else:
        win.blit(text, (20, 13))

    # player
    p.draw(win)
    pygame.display.update()


# decrement score by 1 each second
DECREASE = pygame.USEREVENT + 1
pygame.time.set_timer(DECREASE, 1000)

# moving obstacle
random.seed(10)
pac = []
for i in range(5):
    p = pacman(random.random()*500 + 40, 70+i*150)
    pac.append(p)

# fixed obstacle
random.seed(3)
x_fix = [1, 2, 3, 4, 5, 6]
for i in range(6):
    x_fix[i] = random.random()*900
fix = []
for i in range(6):
    s = stone(x_fix[i], i*150-5)
    fix.append(s)
# player
p1 = ghost(470, 750, 30, 30, 0)
p2 = ghost(470, 20, 30, 30, 1)
p = p1

clock = pygame.time.Clock()
run = True
rounds = 0
cross = []
score1_round = []  # player1 score
score2_round = []  # player2 score
start_time = pygame.time.get_ticks()


# main
while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == DECREASE:
            p.score -= 1
    if rounds >= 2:
        break

    # collision
        # with fixed
    for i in fix:
        if (p.hitbox[1] > i.hitbox[1] and p.hitbox[1] < i.hitbox[1] +
                i.hitbox[3]) or (p.hitbox[1] + p.hitbox[3] > i.hitbox[1] and
                    p.hitbox[1] + p.hitbox[3] < i.hitbox[1] + i.hitbox[3]):
            if ((p.hitbox[0] > i.hitbox[0] and p.hitbox[0] < i.hitbox[0] + i.hitbox[2]) or (p.hitbox[0] + p.hitbox[2] > i.hitbox[0] and p.hitbox[0] + p.hitbox[2] < i.hitbox[0] + i.hitbox[2])):

                p.collide(0)  # make sound and decrease score
                pygame.time.delay(400)
                # resetting position
                p1.x = p2.x = 470
                p1.y = 750
                p2.y = 20
                if p == p1:
                    score1_round.append(p.score)
                    p = p2
                else:
                    score2_round.append(p.score)
                    p = p1

                cross = []  # scoring for next player happens without bugs
                # moving obstacle velocity
                rounds += 0.5
                if rounds % 1 == 0 and rounds != 0:
                    for i in range(5):
                        if pac[i].vel < 0:
                            pac[i].vel -= 2.5
                        else:
                            pac[i].vel += 2.5

                break

        # with moving
    for i in pac:
        if (p.hitbox[1] > i.hitbox[1] and p.hitbox[1] < i.hitbox[1] + i.hitbox[3]) or (p.hitbox[1] + p.hitbox[3] > i.hitbox[1] and p.hitbox[1] + p.hitbox[3] < i.hitbox[1] + i.hitbox[3]):
            if ((p.hitbox[0] > i.hitbox[0] and p.hitbox[0] < i.hitbox[0] + i.hitbox[2]) or (p.hitbox[0] + p.hitbox[2] > i.hitbox[0] and p.hitbox[0] + p.hitbox[2] < i.hitbox[0] + i.hitbox[2])):

                p.collide(0)  # make sound and decrease score
                pygame.time.delay(400)
                # resetting position
                p1.x = p2.x = 470
                p1.y = 750
                p2.y = 20
                if p == p1:
                    score1_round.append(p.score)
                    p = p2
                else:
                    score2_round.append(p.score)
                    p = p1

                cross = []  # scoring for next player happens without bugs
                # moving obstacle velocity
                rounds += 0.5
                if rounds % 1 == 0 and rounds != 0:
                    for i in range(5):
                        if pac[i].vel < 0:
                            pac[i].vel -= 2.5
                        else:
                            pac[i].vel += 2.5

                break

    # score increment for crossing obstacles
    for i in range(6):
        if p.hitbox[1] > i*140 and p.hitbox[1] + p.hitbox[3] < i*140 + 50:
            if i not in cross:
                if i == 5:
                    p.score += 5
                else:
                    p.score += 15
                cross.append(i)
                break

    # player crossed
    if (p.y < 18 and p == p1) or (p.y > 740 and p == p2 and rounds != 0):
        rounds += 0.5
        if rounds % 1 == 0 and rounds != 0:
            for i in range(5):
                if pac[i].vel < 0:
                    pac[i].vel -= 2.5
                else:
                    pac[i].vel += 2.5

        p1.x = p2.x = 470
        p1.y = 750
        p2.y = 20
        if p == p1:
            score1_round.append(p.score)
            p = p2
        else:
            score2_round.append(p.score)
            p = p1
        cross = []  # scoring for p2 happens

    # moving ghost main
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT] and p.x > p.vel:
        p.x -= p.vel
        p.left = True
        p.right = False
        p.up = False
        p.down = False

    # if not(p1.y > 770 and p1.x + p1.vel > 850):
    elif keys[pygame.K_RIGHT] and p.x < 970 - p.width - p.vel:
        p.x += p.vel
        p.left = False
        p.right = True
        p.up = False
        p.down = False

    elif keys[pygame.K_UP] and p.y > p.vel:
        p.y -= p.vel
        p.left = False
        p.right = False
        p.up = True
        p.down = False

    elif keys[pygame.K_DOWN] and p.y < 800 - p.height - p.vel:
        p.y += p.vel
        p.left = False
        p.right = False
        p.up = False
        p.down = True
    else:
        p.left = False
        p.right = False
        p.up = False
        p.down = False

    drawobj()  # drawing everything
    clock.tick(144)
# main end - score calcuated

# show the winner
run = True
# winner
if score1_round[0] + score1_round[1] > score2_round[0] + score2_round[1]:
    winner = 1
else:
    winner = 2


def draw_end():
    # table
    c1 = hfont.render('Round-1', 1, white)
    c2 = hfont.render('Round-2', 1, white)
    r1 = hfont.render('Player-1', 1, white)
    r2 = hfont.render('Player-2', 1, white)
    # values in table
    val11 = vfont.render(str(score1_round[0]), 1, white)
    val12 = vfont.render(str(score1_round[1]), 1, white)
    val21 = vfont.render(str(score2_round[0]), 1, white)
    val22 = vfont.render(str(score2_round[1]), 1, white)
    # declaration
    text_winner = vfont.render(text_end_1 + str(winner) + text_end_2, 1, white)
    # exit
    text_exit = font.render('press Enter to exit', 1, white)

    win.fill((0, 0, 0))
    win.blit(c1, (300, 100))
    win.blit(c2, (600, 100))
    win.blit(r1, (50, 200))
    win.blit(r2, (50, 400))

    win.blit(val11, (350, 220))
    win.blit(val12, (650, 220))
    win.blit(val21, (350, 420))
    win.blit(val22, (650, 420))

    win.blit(text_winner, (350, 550))
    win.blit(text_exit, (650, 700))
    pygame.display.update()


while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    draw_end()
    keys = pygame.key.get_pressed()
    if keys[pygame.K_RETURN]:
        run = False

pygame.quit()
