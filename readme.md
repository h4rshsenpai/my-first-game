# River Crossing Game
# -Harshdeep Singh, 2019115001
for ISS assignment 3, IIITH 2020 sem-2

### Installation

The game requires pygame to run
to install:
```sh
$pip3 install pygame 
```
Run the game with the following command 
```sh
$python3 game.py
```

### Rounds 
- 2 rounds, each round invloves player-1 having a go followed by player two
- player-1 starts from bottom. player-2 from top 
- objective - reach the other side of the screen
- colliding with any enemy ( fixed/moving ) or reaching the end marks the end of that player's round
- player is safe on land strips in bw partitions of the river

### Enemies
- 2 types of enemies - fixed on land and moving 
- 5 moving enemies (pac-mans xD) flowinf in the river, speed increases in the next round 
- 6 fixed enemies (insects, ikr who even likes insects? * vomit *) on the land strips so that the player cant walk freely on land

### Scoring 
- scoring is based on safely crossing a fixed obstacle(player gains 5 points for each), a moving obstacle (player gains 10 points for each), and time taken by player to finish/die (every second passed is a single point cut from the players current score)
- after two rounds, scores for both player for both rounds are displayed and the winner (player with the higher score) is displayed

### Additional Information
- insects make a growling sound when player collides with it 
- pacmans make a lulululu sound when player collides with it (personal trademark of mine)
